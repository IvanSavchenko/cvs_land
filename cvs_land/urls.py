"""cvs_land URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

import cvs_land.views


application_views = [
    url(regex=r'^$',
        view=cvs_land.views.IndexView.as_view(),
        name='index'),
    url(regex=r'^about/$',
        view=cvs_land.views.AboutView.as_view(),
        name='about')
]

default = [
    url(r'^admin/', admin.site.urls),
    static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)[0]
]

urlpatterns = application_views + default
