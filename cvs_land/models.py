"""Application models."""

from django.db import models


class Blog(models.Model):
    """Blog table."""

    author_first_name = models.CharField(
        max_length=30,
        blank=True,
        null=True,
        default='Unknown')

    author_last_name = models.CharField(
        max_length=30,
        blank=True,
        default='Unknown')

    subject = models.CharField(
        max_length=30,
        blank=True,
        default='No subject')

    preview = models.CharField(
        max_length=1000)

    content = models.TextField()

    class Meta:
        """Meta parameters."""

        ordering = ['subject']
        verbose_name = 'blog'
        verbose_name_plural = 'blogs'

    def __unicode__(self):
        """New return representation."""
        return '{first_name} {last_name} - {subject}'.format(
            first_name=self.author_first_name,
            last_name=self.author_last_name,
            subject=self.subject)
